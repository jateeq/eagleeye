package com.example.eagleeye;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Main extends Activity implements SurfaceHolder.Callback {
	private SurfaceView camSurface;
	private Camera camera;
    private Button takeFrame;
    private Button analyzePart;
    private SurfaceHolder holder;
    private boolean picture_taken = false;
    private boolean cameraConfigured = false;
    private PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            Log.d("Log", "onPictureTaken - raw");
        }
    }; 
    private ShutterCallback shutterCallback =  new ShutterCallback() {
        public void onShutter() {
            Log.i("Log", "onShutter'd");
        }
    };
    private PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream outStream = null;
            
            try {
            	currentImageDir = String.format(
            			Environment.getExternalStorageDirectory() + "/%d.jpg", System.currentTimeMillis());
                outStream = new FileOutputStream(currentImageDir);
                outStream.write(data);
                outStream.close();
                Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
            Log.d("Log", "onPictureTaken - jpeg");
            

		//	camera.startPreview();
			
			inPreview = true;
        }
    };
    final int TAKE_PICTURE = 115;
    private String currentImageDir;
    private boolean inPreview=false;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        takeFrame = (Button) findViewById(R.id.takeFrame);
        analyzePart = (Button) findViewById(R.id.analyzePart);
        
        camSurface = (SurfaceView) findViewById(R.id.camSurface);
        
        holder = camSurface.getHolder();
        holder.addCallback(this);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        takeFrame.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
			
				camera.takePicture(shutterCallback, rawCallback, jpegCallback);
		        
			}
 
		});
        
        analyzePart.setOnClickListener(new OnClickListener() {
        	 
			@Override
			public void onClick(View arg0) {
			    // give the drawble resource for the ImageView
	            Bitmap myBitmap = BitmapFactory.decodeFile(currentImageDir);
			    Matrix matrix = new Matrix();
			     matrix.postRotate(90);
			     Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);
			  // add both the Views TextView and ImageView in layout
	            LinearLayout layout = new LinearLayout(analyzePart.getContext());
			    ImageView img = (ImageView) findViewById(R.id.image);
			    img.setImageBitmap(myBitmap);
	            layout.setBackgroundResource(R.color.black);
				Dialog dialog = new Dialog(analyzePart.getContext());
				dialog.setContentView(R.layout.result);
				dialog.show();
			}
 
		});

    }


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		if (inPreview)
		{
			camera.stopPreview();
		}
		
		// TODO Auto-generated method stub
		Camera.Parameters p = camera.getParameters();
		Camera.Size size = getBestPreviewSize(width,height,camera.getParameters());
		p.setPreviewSize(size.width, size.height);
		camera.setDisplayOrientation(90);
		
		camera.setParameters(p);

		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException e) {
			e.printStackTrace();
		}

		camera.startPreview();
		
		inPreview = true;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera = Camera.open();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera.stopPreview();
		inPreview = false;
		camera.release();
	}
    
    private Camera.Size getBestPreviewSize(int width, int height,
    	    Camera.Parameters parameters) 
    {
		Camera.Size result=null;
		
		for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
			if (size.width<=width && size.height<=height) {
				if (result==null) {
					result=size;
				}
				else {
					int resultArea=result.width*result.height;
					int newArea=size.width*size.height;
					
					if (newArea>resultArea) {
						result=size;
					}
				}
			}
		}
		
		return(result);
    }
}
