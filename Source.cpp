#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdio.h>
#include <math.h>
#include <iostream>

using namespace cv;
using namespace std;

void help()
{
	cout << "\nThis program demonstrates line finding with the Hough transform.\n"
		"Usage:\n"
		"./houghlines <image_name>, Default is pic1.jpg\n" << endl;
}

double Distance(double dX0, double dY0, double dX1, double dY1)
{
	return sqrt((dX1 - dX0)*(dX1 - dX0) + (dY1 - dY0)*(dY1 - dY0));
}

int main(int argc, char** argv)
{
	const char* filename = argc >= 2 ? argv[1] : "pic1.jpg";

	Mat src = imread(filename, 0);
	if (src.empty())
	{
		help();
		cout << "can not open " << filename << endl;
		return -1;
	}

	Mat dst, cdst;
	Canny(src, dst, 50, 200, 3);
	cvtColor(dst, cdst, CV_GRAY2BGR);

#if 0
	vector<Vec2f> lines;
	HoughLines(dst, lines, 1, CV_PI / 180, 100, 0, 0);

	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, CV_AA);
	}
#else
	vector<Vec4i> lines;
	HoughLinesP(dst, lines, 1, CV_PI / 180 * 1.5, 50, 50, 10);
	int lineGraph[1000] = { 0 };

	/*Display Lines*/
	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		line(src, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, CV_AA);
	}
	/*Calculate Angles*/
	for (size_t i = 0; i < lines.size(); i++)
	{
		for (size_t j = 0; j < lines.size(); j++)
		{
			if (i != j && lineGraph[i*j] == 0)
			{
				double startStartPtsDist = 0;
				double startEndPtsDist = 0;
				double endStartPtsDist = 0;
				double endEndPtsDist = 0;
				startStartPtsDist = Distance(lines[i][0], lines[i][1], lines[j][0], lines[j][1]);
				startEndPtsDist = Distance(lines[i][0], lines[i][1], lines[j][2], lines[j][3]);
				endStartPtsDist = Distance(lines[i][2], lines[i][3], lines[j][0], lines[j][1]);
				endEndPtsDist = Distance(lines[i][2], lines[i][3], lines[j][2], lines[j][3]);

				/*if two lines are between 10 pixel distance from each other then calculate the angle
				between the two lines*/
				double thetaLine1 = 0;
				double thetaLine2 = 0;
				double theta = 0;
				double mLine1 = 0;
				double mLine2 = 0;
				double bLine1 = 0;
				double bLine2 = 0;
				double interceptX = 0;
				double interceptY = 0;
				double textX = 0;
				double textY = 0;
				char theta_str[10];
				if (startStartPtsDist < 30 || startEndPtsDist < 30 || endStartPtsDist < 30 || endEndPtsDist < 30) {
					if (startStartPtsDist < 30)
					{
						thetaLine1 = atan2((lines[i][3] - lines[i][1]), (lines[i][2] - lines[i][0]));
						thetaLine2 = atan2((lines[j][3] - lines[j][1]), (lines[j][2] - lines[j][0]));
						textX = lines[i][0];
						textY = lines[i][1];
					}
					else if (startEndPtsDist < 30)
					{
						thetaLine1 = atan2((lines[i][3] - lines[i][1]), (lines[i][2] - lines[i][0]));
						thetaLine2 = atan2((lines[j][1] - lines[j][3]), (lines[j][0] - lines[j][2]));

						textX = lines[i][0];
						textY = lines[i][1];
					}
					else if (endStartPtsDist < 30)
					{
						thetaLine1 = atan2((lines[i][1] - lines[i][3]), (lines[i][0] - lines[i][2]));
						thetaLine2 = atan2((lines[j][3] - lines[j][1]), (lines[j][2] - lines[j][0]));
						textX = lines[i][2];
						textY = lines[i][3];
					}
					else if (endEndPtsDist < 30)
					{
						thetaLine1 = atan2((lines[i][1] - lines[i][3]), (lines[i][0] - lines[i][2]));
						thetaLine2 = atan2((lines[j][1] - lines[j][3]), (lines[j][0] - lines[j][2]));
						textX = lines[i][2];
						textY = lines[i][3];
					}
					double XDiffLine1 = lines[i][2] - lines[i][0];
					double XDiffLine2 = lines[j][2] - lines[j][0];
					/*Calculate Intercept*/
					if (XDiffLine1 < 0.001)
						XDiffLine1 = 0.001;
					if (XDiffLine2 < 0.001)
						XDiffLine2 = 0.001;
					mLine1 = (lines[i][3] - lines[i][1]) / XDiffLine1;
					mLine2 = (lines[j][3] - lines[j][1]) / XDiffLine2;
					bLine1 = lines[i][3] - mLine1 * lines[i][2];
					bLine2 = lines[j][3] - mLine2 * lines[j][2];
					interceptX = (bLine2 - bLine1) / (mLine1 - mLine2);
					interceptY = mLine1 * interceptX + bLine1;
					printf("%f, ", interceptX);
					printf("%f; ", interceptY);
					ellipse(src, cvPoint(interceptX, interceptY), cvSize(10, 10), 0, thetaLine1 * 180 / CV_PI, thetaLine2 * 180 / CV_PI, cvScalar(255, 0, 0), 1, CV_AA, 0);

					theta = (thetaLine1 - thetaLine2) * 180 / CV_PI;
					theta = fmod((theta + 180), 360) - 180;
					if (theta < 0)
					{
						theta = theta * -1;
					}
					sprintf(theta_str, "%1.1f", theta);
					putText(src, theta_str, cvPoint(textX + 15, textY), FONT_HERSHEY_COMPLEX_SMALL, 0.8, cvScalar(255, 0, 0), 1, CV_AA, false);
					lineGraph[i*j] = 1;
				}
			}
		}
	}

#endif
	imshow("source", src);
	imshow("detected lines", cdst);

	waitKey();

	return 0;
}